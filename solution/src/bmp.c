#include "bmp.h"
#include <malloc.h>

enum read_status from_bmp(FILE* in_file, struct image* image) {
    //read bmp header
    struct bmp_header bmp_header = create_header_from_bmp(in_file);
    //check header
    check_bmp(bmp_header);
    //some vars
    const uint32_t width = bmp_header.biWidth;
    const uint32_t height = bmp_header.biHeight;
    //update image
    update_image(width, height, image);
    //read all pixels
    read_pixels(width,height,image,in_file);

    return READ_OK;
}

enum read_status read_pixels(uint32_t width, uint32_t height, struct image* image, FILE* in_file) {
    //padding
    const uint8_t padd = padding(width);
    //trash var
    char trash[] = {"trash"};
    //read all pixels by lines
    for (size_t i = 0; i < height; i++) {
        //read line of file
        fread(image->data + width * i, sizeof(struct pixel), width, in_file);
        //make padding space (miss padd bytes)
        fread(&trash, 1, padd, in_file);
    }
    return READ_OK;
}

enum read_status check_bmp (struct bmp_header bmp_header) {
    if (bmp_header.bfType != 0x4D42) { return READ_INVALID_SIGNATURE; }
    if (bmp_header.biBitCount != 24) { return READ_INVALID_BITS; }
    return READ_OK;
}

void update_image(const uint32_t width, const uint32_t height, struct image* image) {
    image->width = width;
    image->height = height;
    image->data = malloc(width * height * sizeof(struct pixel));
}

struct bmp_header create_header_from_bmp(FILE* in) {
    //bmp header
    struct bmp_header bmp_header_file = {0};
    //read header bytes
    fread(&(bmp_header_file),sizeof(struct bmp_header), 1, in);
    return bmp_header_file;
}

uint8_t padding(uint32_t width) {
    //byte size of width
    const size_t bytes = width * (sizeof(struct pixel));
    //padding
    const size_t padd = bytes % 4;
    if (padd != 0) {
        //real padding
        return 4 - padd;
    }

    return 0;
}

enum write_status to_bmp(FILE* out, struct image const* image) {
    //make header
    struct bmp_header bmp_header = create_bmp_header(image);
    //write header
    fwrite(&bmp_header, sizeof(struct bmp_header), 1, out);
    //some vars
    const uint32_t width = bmp_header.biWidth;
    const uint32_t height = bmp_header.biHeight;
    //write all pixels in file
    write_pixels(width,height,image,out);

    return WRITE_OK;
}

enum write_status write_pixels(uint32_t width, uint32_t height, struct image const* image, FILE* out) {
    //trash var
    char trash[] = {"trash"};
    //padding
    const uint8_t padd = padding(width);
    for (size_t i = 0; i < height; i++) {
        //write each line
        if (fwrite(image->data + i * width, sizeof(struct pixel), width, out) == 0) {
            return WRITE_ERROR;
        }
        // write padding if it needs
        if (padd) fwrite(&trash, 1, padd, out);
    }
    return WRITE_OK;
}

struct bmp_header create_bmp_header(struct image const* image) {
    //var header
    struct bmp_header bmp_header_file = {0};
    //some vars
    const uint32_t width = image->width;
    const uint32_t height = image->height;
    //some calculators
    const uint32_t bfileSize = sizeof(struct bmp_header) + height * (width * sizeof(struct pixel) + padding(width));
    const uint32_t biWidth = width;
    const uint32_t biHeight = height;
    const uint32_t biSizeImage = (width + height) * sizeof(struct pixel) + height * padding(width);
    //write to header
    bmp_header_file.bfType = 0x4D42;
    bmp_header_file.bfileSize = bfileSize;
    bmp_header_file.bfReserved = 0;
    bmp_header_file.bOffBits = 54;
    bmp_header_file.biSize = 40;
    bmp_header_file.biWidth = biWidth;
    bmp_header_file.biHeight = biHeight;
    bmp_header_file.biPlanes = 1;
    bmp_header_file.biBitCount = 24;
    bmp_header_file.biCompression = 0;
    bmp_header_file.biSizeImage = biSizeImage;
    bmp_header_file.biXPelsPerMeter = 0;
    bmp_header_file.biYPelsPerMeter = 0;
    bmp_header_file.biClrUsed = 0;
    bmp_header_file.biClrImportant = 0;

    return bmp_header_file;
}
