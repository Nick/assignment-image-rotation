#ifndef IMAGE_TRANSFORMER_CLOSE_FILE_H
#define IMAGE_TRANSFORMER_CLOSE_FILE_H
#include <stdio.h>

enum close_status{
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status close_file(FILE** file);

#endif //IMAGE_TRANSFORMER_CLOSE_FILE_H
