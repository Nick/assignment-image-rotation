#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include <stddef.h>
#include <stdint.h>

struct image rotate_by_90_left(struct image const original_image );
size_t get_index_of_pixel(size_t i, size_t j, uint32_t width, uint32_t height);

#endif //IMAGE_TRANSFORMER_ROTATE_H
