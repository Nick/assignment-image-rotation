#ifndef IMAGE_TRANSFORMER_BMP_HEADER_H
#define IMAGE_TRANSFORMER_BMP_HEADER_H

#include "image.h"
#include "read_write_enums.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status from_bmp( FILE* in, struct image* image );
enum write_status to_bmp( FILE* out, struct image const* image );

enum read_status check_bmp (struct bmp_header bmp_header);

enum read_status read_pixels(uint32_t width, uint32_t height, struct image* image, FILE* in_file);
enum write_status write_pixels(uint32_t width, uint32_t height, struct image const* image, FILE* out);

#endif //IMAGE_TRANSFORMER_BMP_HEADER_H
