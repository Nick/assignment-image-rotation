#ifndef IMAGE_TRANSFORMER_OPEN_FILE_H
#define IMAGE_TRANSFORMER_OPEN_FILE_H

#include <stdbool.h>
#include <stdio.h>


enum open_status{
    OPEN_OK = 0,
    OPEN_ERROR = 1,
    OPEN_ERROR_FILE_PATH = 2,
    OPEN_ERROR_MODE = 3
};

enum open_status open_file(FILE** file, char* file_path, char* mode);
bool mode_correct(const char* mode);

#endif //IMAGE_TRANSFORMER_OPEN_FILE_H
