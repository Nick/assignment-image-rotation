#ifndef IMAGE_TRANSFORMER_READ_WRITE_ENUMS_H
#define IMAGE_TRANSFORMER_READ_WRITE_ENUMS_H
/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

#endif //IMAGE_TRANSFORMER_READ_WRITE_ENUMS_H
