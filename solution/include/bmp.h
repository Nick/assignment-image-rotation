#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "bmp_header.h"

struct bmp_header create_header_from_bmp(FILE* in );
struct bmp_header create_bmp_header(struct image const* image);

void update_image(const uint32_t width, const uint32_t height, struct image* image);

uint8_t padding(uint32_t width);

#endif //IMAGE_TRANSFORMER_BMP_H
