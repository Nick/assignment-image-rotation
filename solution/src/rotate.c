#include "image.h"
#include "rotate.h"
#include <malloc.h>

struct image rotate_by_90_left(struct image const original_image){
    //some vars
    const uint32_t original_width = original_image.width;
    const uint32_t original_height = original_image.height;
    //const rotated_image to return
    struct image rotated_image =
            {
             .height = original_width,
             .width  = original_height,
             .data   = malloc(original_width * original_height * sizeof(struct pixel))
            };
    //make rotation like:
    //1 2 3 4
    //5 6 7 8
    //to:
    //4 8
    //3 7
    //2 6
    //1 5
    //index of original_image.data
    size_t position_from = 0;
    for (size_t i = 0; i < rotated_image.height; i++){
        for (size_t j = 0; j < rotated_image.width; j++){
            size_t position_to = get_index_of_pixel(i,j,original_width,original_height);
            rotated_image.data[position_from] = original_image.data[position_to];
            position_from++;
        }
    }

    return rotated_image;
}

size_t get_index_of_pixel(size_t i, size_t j, uint32_t width, uint32_t height) {
    return (height - j - 1) * width + i;
}
