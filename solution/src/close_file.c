#include "close_file.h"

enum close_status close_file(FILE** file) {
    //close file
    if (fclose(*file)==0) { return CLOSE_OK; }
    else { return CLOSE_ERROR; }
}
