#include "open_file.h"
#include <stdbool.h>

 enum open_status open_file(FILE** file, char* file_path, char* mode) {



    //mode error
    if (mode == NULL) { return OPEN_ERROR_MODE; }
    if (file_path != NULL) {
        //open file by path and mode
        bool is_mode_correct = mode_correct(mode);
        if (is_mode_correct) {
            *file = fopen(file_path, mode);
        } else {
            return OPEN_ERROR_MODE;
        }
    }
    else { return OPEN_ERROR_FILE_PATH; }

    return OPEN_OK;
}

bool mode_correct(const char* mode) {
    char* modes[18] = {"r", "w", "a", "rb", "wb", "ab",
                       "r+", "w+","a+","r+b","w+b",
                       "a+b","rt","wt", "at", "r+t",
                       "w+t", "a+t" };

    for (size_t i = 0; i < 18; i = i + 1) {
        if (*modes[i] == *mode) {
            return true;
        }
    }
    return false;
}
