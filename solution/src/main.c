#include "bmp.h"
#include "close_file.h"
#include "image.h"
#include "open_file.h"
#include "rotate.h"
#include <malloc.h>

int main( int argc, char** argv ) {(void) argc; (void) argv; // supress 'unused parameters' warning
    //path from/to
    char* in_path = argv[1];
    char* out_path = argv[2];
    //file IN/OUT
    FILE* in_file;
    FILE* out_file;
    //original image
    struct image image = {0};
    //open files
    open_file(&in_file, in_path, "rb");
    open_file(&out_file, out_path, "wb");
    //main part1
    from_bmp(in_file, &image);
    //rotated image
    struct image rotated_image = rotate_by_90_left(image);
    //main part2
    to_bmp(out_file, &rotated_image);
    //close files
    close_file(&in_file);
    close_file(&out_file);
    //frees
    free(image.data);
    free(rotated_image.data);

    return 0;
}
